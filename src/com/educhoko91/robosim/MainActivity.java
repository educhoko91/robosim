package com.educhoko91.robosim;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */

	int[][] map;
	int columns;
	int rows;
	int obstacles;
	GridLayout layout;
	RelativeLayout frame;
    EditText code;

	public static final int ROBOT = 1;
	public static final int EMPTY = 0;
	public static final int OBSTACLE = 2;
	public static final int FINISH = 3;

    public static final int INDEX_OUT_OF_BOUNDS=-1;
    public static final int OBSTACLE_COLLITION=-2;
    public static final int FINISHED = 2;
    public static final int OK = 1;


    private int robotColumn;
    private int robotRow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		frame = (RelativeLayout) findViewById(R.id.frame);
        code = (EditText) findViewById(R.id.code);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, "Generate Map");
		menu.add(0, 1, 1, "Play");
		/*
		 * menu.add("Generate Map"); menu.add("Play");
		 */
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getOrder() == 0) {
			final Dialog dialog = new Dialog(this);
			dialog.setContentView(R.layout.generate_dialog);
			dialog.setTitle("Generate Map");
			Button generate = (Button) dialog.findViewById(R.id.generate);
			Button cancel = (Button) dialog.findViewById(R.id.cancel);
			cancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.cancel();
				}
			});

			generate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					EditText columns = (EditText) dialog
							.findViewById(R.id.columns);
					EditText rows = (EditText) dialog.findViewById(R.id.rows);
					EditText obstacles = (EditText) dialog
							.findViewById(R.id.obstacles);
					if (columns.getText().length() == 0)
						Toast.makeText(MainActivity.this,
								"Columns can't be empty", Toast.LENGTH_LONG)
								.show();
					else if (rows.getText().length() == 0) {
						Toast.makeText(MainActivity.this,
								"Rows can't be empty", Toast.LENGTH_LONG)
								.show();
					} else if (rows.getText().length() == 0) {
						Toast.makeText(MainActivity.this,
								"Obstacles can't be empty", Toast.LENGTH_LONG)
								.show();
					} else {
						MainActivity.this.columns = Integer.parseInt(String
								.valueOf(columns.getText()));
						MainActivity.this.rows = Integer.parseInt(String
								.valueOf(rows.getText()));
						MainActivity.this.obstacles = Integer.parseInt(String
								.valueOf(obstacles.getText()));
						initMap();
						dialog.dismiss();
					}

				}
			});
			dialog.show();
		}
        else  if(item.getOrder() == 1) {
            initMap();
            parser();
        }
		return true;
	}

    private void parser() {
        String code = MainActivity.this.code.getText().toString();


        for(String line:code.split("\\n")) {
            Matcher m = Pattern.compile("[uU] ([0-9]+)|[dD] ([0-9]+)|[lL] ([0-9]+)|[rR] ([0-9]+)").matcher(line);

            if(m.find()) {
                if(m.group(1)!=null) {
                    if(evaluateMove(goUp(Integer.parseInt(m.group(1))))){
                        break;
                    }

                }
                if(m.group(2)!=null) {
                    if(evaluateMove(goDown(Integer.parseInt(m.group(2)))) ){
                        break;
                    }
                }
                if(m.group(3)!=null) {
                    if(evaluateMove(goLeft(Integer.parseInt(m.group(3))))) {
                        break;
                    }
                }
                if(m.group(4)!=null) {
                    if(evaluateMove(goRight(Integer.parseInt(m.group(4))))) {
                        break;
                    }

                }

            }
            else {
                Toast.makeText(MainActivity.this,"Compilation Failed!!!",Toast.LENGTH_LONG).show();
                initMap();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        generateMap();
    }

    public boolean evaluateMove(int x) {
        switch (x) {
            case INDEX_OUT_OF_BOUNDS:
                Toast.makeText(this,"Out of the Map",Toast.LENGTH_LONG).show();
                return true;
            case OBSTACLE_COLLITION:
                Toast.makeText(this,"The robot cash with one obstacle",Toast.LENGTH_LONG).show();
                return true;
            case FINISHED:
                Toast.makeText(this,"The robot made it!!!!!",Toast.LENGTH_LONG).show();
                return true;
            case OK:
                return false;
        }
        return true;
    }


	private void initMap() {
		map = new int[columns][rows];
		map[0][rows - 1] = ROBOT;
		map[columns - 1][0] = FINISH;
        robotColumn = 0;
        robotRow = rows-1;
		generateMap();
	}

	private void generateMap() {
        frame.removeAllViewsInLayout();
		layout = new GridLayout(this);
		layout.setColumnCount(columns);
		layout.setRowCount(rows);

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				ImageView imageView = new ImageView(this);
				GridLayout.LayoutParams params;
				switch (map[j][i]) {
				case EMPTY:
					imageView.setBackgroundColor(Color.WHITE);
					break;
				case ROBOT:
					//imageView.setBackgroundColor(Color.BLACK);
                    imageView.setImageResource(R.drawable.sprite);
					break;
				case OBSTACLE:
					imageView.setBackgroundColor(Color.BLUE);
					break;
				case FINISH:
					imageView.setBackgroundColor(Color.GREEN);
					break;
				}
				params = new GridLayout.LayoutParams();
				params.setMargins(5, 5, 5, 5);
				params.width = 20;
				params.height = 20;
                //params.setGravity(Gravity.NO_GRAVITY);
				layout.addView(imageView, params);
				// layout.addView(imageView,50,50);
			}
		}
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
       //params.


		frame.addView(layout);
        frame.setGravity(Gravity.RIGHT);

	}

    private int goUp(int x) {
        try {
            map[robotColumn][robotRow] = 0;
            robotRow -= x;
            if (map[robotColumn][robotRow] == OBSTACLE) {
                return OBSTACLE_COLLITION;
            }
            if (map[robotColumn][robotRow] == FINISH) {
                return FINISHED;
            }
            map[robotColumn][robotRow] = 1;
            return OK;
        } catch (IndexOutOfBoundsException e) {
            return INDEX_OUT_OF_BOUNDS;
        }
    }

    private int goDown(int x) {
        try {
            map[robotColumn][robotRow] = 0;
            robotRow += x;
            if (map[robotColumn][robotRow] == OBSTACLE) {
                return OBSTACLE_COLLITION;
            }
            if (map[robotColumn][robotRow] == FINISH) {
                return FINISHED;
            }
            map[robotColumn][robotRow] = 1;
            return OK;
        } catch (IndexOutOfBoundsException e) {
            return INDEX_OUT_OF_BOUNDS;
        }
    }
    private int goRight(int x) {
        try {
            map[robotColumn][robotRow] = 0;
            robotColumn += x;
            if (map[robotColumn][robotRow] == OBSTACLE) {
                return OBSTACLE_COLLITION;
            }
            if (map[robotColumn][robotRow] == FINISH) {
                return FINISHED;
            }
            map[robotColumn][robotRow] = 1;
            return OK;
        } catch (IndexOutOfBoundsException e) {
            return INDEX_OUT_OF_BOUNDS;
        }
    }

    private int goLeft(int x) {
        try {
            map[robotColumn][robotRow] = 0;
            robotColumn -= x;
            if (map[robotColumn][robotRow] == OBSTACLE) {
                return OBSTACLE_COLLITION;
            }
            if (map[robotColumn][robotRow] == FINISH) {
                return FINISHED;
            }
            map[robotColumn][robotRow] = 1;
            return OK;
        } catch (IndexOutOfBoundsException e) {
            return INDEX_OUT_OF_BOUNDS;
        }
    }


}


